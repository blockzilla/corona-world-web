$(document).ready(function () {
    Highcharts.chart('chart1', {
        title: {
            text: 'Global Cases BY Country'
        },

        subtitle: {
            text: 'Source: Johns Hopkins CSSE'
        },

        yAxis: {
            title: {
                text: 'Number of Cases'
            }
        },

        xAxis: {
            categories: ['Jan', 'Feb', 'March', 'April'],
            tickmarkPlacement: 'on',
            title: {
                enabled: false
            },
            accessibility: {
                rangeDescription: 'Range: 2020 to 2020'
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        // plotOptions: {
        //     series: {
        //         label: {
        //             connectorAllowed: false
        //         },
        //         pointStart: 2020
        //     }
        // },

        series: [{
            name: 'USA',
            data: [0, 30664, 33645, 356645]
        }, {
            name: 'Spain',
            data: [0, 11000, 12500, 135032]
        }, {
            name: 'Italy',
            data: [0, 24377, 32147, 132547]
        }, {
            name: 'Germany',
            data: [0, 22452, 34400, 101525]
        }, {
            name: 'France',
            data: [0, 11816, 18274, 98010]
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });

    Highcharts.chart('chart2', {
        chart: {
            type: 'area',
        },
        title: {
            text: 'Cases over time'
        },
        subtitle: {
            text: 'Source: Johns Hopkins CSSE'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'March', 'April'],
            tickmarkPlacement: 'on',
            title: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                text: ''
            },
            labels: {
                formatter: function () {
                    return this.value / 1000;
                }
            }
        },
        tooltip: {
            split: true,
            valueSuffix: ' thousand'
        },
        plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },
        series: [{
            name: 'Confirmed',
            data: [947, 200000, 700000, 1329437]
        }, {
            name: 'Deaths',
            data: [133, 10000, 50000, 73989]
        }, {
            name: 'Recovered',
            data: [408, 547, 150000, 284801]
        }, {
            name: 'Active',
            data: [156, 20000, 15000, 970647]
        }]
    });
});