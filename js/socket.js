let Socket = {
    totalCountries: 195,
    map: null,
    isMobile: ($(window).width() <= 640),
    mapMargin: 30,
    markerRadius: 4,
    mapBorderColor: '#303030',
    mapColor: '#101010',
    theme: Highcharts.theme = {
        colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
            '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        chart: {
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                stops: [
                    [0, '#2a2a2b'],
                    [1, '#3e3e40']
                ]
            },
            style: {
                fontFamily: '\'Unica One\', sans-serif'
            },
            plotBorderColor: '#606063',
            panning: true
        },
        title: {
            style: {
                color: '#E0E0E3',
                textTransform: 'uppercase',
                fontSize: '20px'
            }
        },
        subtitle: {
            style: {
                color: '#E0E0E3',
                textTransform: 'uppercase'
            }
        },
        xAxis: {
            gridLineColor: '#707073',
            labels: {
                style: {
                    color: '#E0E0E3'
                }
            },
            lineColor: '#707073',
            minorGridLineColor: '#505053',
            tickColor: '#707073',
            title: {
                style: {
                    color: '#A0A0A3'
                }
            }
        },
        yAxis: {
            gridLineColor: '#707073',
            labels: {
                style: {
                    color: '#E0E0E3'
                }
            },
            lineColor: '#707073',
            minorGridLineColor: '#505053',
            tickColor: '#707073',
            tickWidth: 1,
            title: {
                style: {
                    color: '#A0A0A3'
                }
            },
            scrollbar: {
                enabled: true
            }
        },
        tooltip: {
            backgroundColor: 'rgba(0, 0, 0, 0.85)',
            style: {
                color: '#F0F0F0'
            }
        },
        plotOptions: {
            series: {
                dataLabels: {
                    color: '#F0F0F3',
                    style: {
                        fontSize: '13px'
                    }
                },
                marker: {
                    lineColor: '#333'
                },
                animation: true,
                stickyTracking: false,
            },
            boxplot: {
                fillColor: '#505053'
            },
            candlestick: {
                lineColor: 'white'
            },
            errorbar: {
                color: 'white'
            },
            panning: true
        },
        legend: {
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            itemStyle: {
                color: '#E0E0E3'
            },
            itemHoverStyle: {
                color: '#FFF'
            },
            itemHiddenStyle: {
                color: '#606063'
            },
            title: {
                style: {
                    color: '#C0C0C0'
                }
            }
        },
        credits: {
            style: {
                color: '#666'
            }
        },
        labels: {
            style: {
                color: '#707073'
            }
        },
        drilldown: {
            activeAxisLabelStyle: {
                color: '#F0F0F3'
            },
            activeDataLabelStyle: {
                color: '#F0F0F3'
            }
        },
        navigation: {
            buttonOptions: {
                symbolStroke: '#DDDDDD',
                theme: {
                    fill: '#505053'
                },
            }
        },
        // scroll charts
        rangeSelector: {
            buttonTheme: {
                fill: '#505053',
                stroke: '#000000',
                style: {
                    color: '#CCC'
                },
                states: {
                    hover: {
                        fill: '#707073',
                        stroke: '#000000',
                        style: {
                            color: 'white'
                        }
                    },
                    select: {
                        fill: '#000003',
                        stroke: '#000000',
                        style: {
                            color: 'white'
                        }
                    }
                }
            },
            inputBoxBorderColor: '#505053',
            inputStyle: {
                backgroundColor: '#333',
                color: 'silver'
            },
            labelStyle: {
                color: 'silver'
            }
        },
        navigator: {
            handles: {
                backgroundColor: '#666',
                borderColor: '#AAA'
            },
            outlineColor: '#CCC',
            maskFill: 'rgba(255,255,255,0.1)',
            series: {
                color: '#7798BF',
                lineColor: '#A6C7ED'
            },
            xAxis: {
                gridLineColor: '#505053'
            }
        },
        scrollbar: {
            barBackgroundColor: '#808083',
            barBorderColor: '#808083',
            buttonArrowColor: '#CCC',
            buttonBackgroundColor: '#606063',
            buttonBorderColor: '#606063',
            rifleColor: '#FFF',
            trackBackgroundColor: '#404043',
            trackBorderColor: '#404043'
        }
    },
    initHandlers: function () {
        $(document).on('click', '.nav-link', this.navLinkHandler);
        this.createMap();
        this.requestPushNotifications();
        return false;
    },
    navLinkHandler: function (e) {
        e.preventDefault();
        el = $(this);
        $('body main').each(function () {
            $(this).hide();
        });
        $('.nav-item').each(function () {
            $(this).removeClass('active');
        });
        var linkData = el.data('tab')
        if (linkData === 'news') {
            $('body').css('overflow-y: scroll')
            $('html, body').animate({ scrollTop: '0px' }, 300);
        } else {
            $('body').css('overflow-y: none')
        }
        if (linkData === 'map') {
            $('#container').highcharts().redraw()
        }
        $('.' + linkData).parent('li').addClass('active');
        $('#' + linkData).show();
    },
    requestPushNotifications: function () {
        if (!Push.Permission.has()) {
            Push.Permission.request();
        }
    },
    initSocketHandlers: function () {
        console.log('init socker handler');
        let socket = io('/', { transports: ['websocket'], upgrade: false });
        socket.on('connect', function () {
            console.log('Socket connected');
        });
        socket.on('disconnect', function (e) {
            console.log('Socket disconnected');
            socket.io.reconnect();
        });
        socket.on('ping', function (data) {
            socket.emit('pong', { beat: 1 });
        });
        socket.on('news', data => this.handleNews(data));
        socket.on('summary', data => this.renderSummary(data));
        socket.on('cases', data => this.renderMap(data));
    },
    pointsToPath: function (from, to, invertArc) {
        if (typeof from === 'undefined' && typeof to === 'undefined') {
            return true
        }
        var arcPointX = (from.x + to.x) / (invertArc ? 1.4 : 1.6),
            arcPointY = (from.y + to.y) / (invertArc ? 1.4 : 1.6);
        return 'M' + from.x + ',' + from.y + 'Q' + arcPointX + ' ' + arcPointY +
            ',' + to.x + ' ' + to.y;
    },
    createMap: function () {
        console.log('create map')
        if (this.isMobile) {
            mapMargin = 0;
            markerRadius = 3;
            mapBorderColor = '#202020';
            mapColor = '#202020';
        }
        Highcharts.setOptions(this.theme);
        Highcharts.wrap(Highcharts.Chart.prototype, 'pan', function (proceed) {
            Highcharts.each(this.yAxis, function (axis) {
                axis.fixTo = null;
            });
            proceed.apply(this, Array.prototype.slice.call(arguments, 1));
        });

        this.map = new Highcharts.Map({
            chart: {
                backgroundColor: null,
                map: 'custom/world',
                renderTo: 'container',
                panning: true
            },
            exporting: {
                enabled: true
            },
            boost: {
                allowForce: true,
                enabled: true,
                useGPUTranslations: true,
                usePreallocated: true,
            },
            title: {
                text: 'COVID-19 Case Data'
            },
            subtitle: {
                text: ''
            },
            mapNavigation: {
                enabled: false,
                enableMouseWheelZoom: false,
                buttonOptions: {
                    align: 'right',
                    verticalAlign: 'middle',
                    x: -10,
                    y: 10
                },
            },
            legend: {
                enabled: true,
                align: 'left',
                verticalAlign: 'middle',
                x: -10,
                y: 10,
                alignColumns: true,
                layout: 'vertical',
                floating: true,
                itemMarginTop: 6,
                itemMarginBottom: 6
            },
            credits: {
                enabled: false
            },
            series: [
                {
                    mapData: Highcharts.maps['custom/world-highres'],
                    borderColor: this.mapBorderColor,
                    nullColor: this.mapColor,
                    name: 'Corona Virus Cases',
                    enableMouseTracking: false,
                    showInLegend: false
                },
                {
                    type: 'mapbubble',
                    name: 'Confirmed',
                    joinBy: ['iso-a3', 'code3'],
                    data: [],
                    minSize: 3,
                    maxSize: '18%',
                    dataLabels: {
                        format: '{point.id}'
                    },
                    tooltip: {
                        backgroundColor: 'none',
                        borderWidth: 0,
                        shadow: false,
                        useHTML: true,
                        padding: 0,
                        pointFormat: '<p class="tooltip-bubble"><span style="font-size:20px">' +
                            '{point.province}, {point.country}</span> <br>' +
                            '<span style="font-size:16px">Confirmed: {point.z}</span><br>' +
                            '<span style="font-size:16px">Recovered: {point.recovered}</span><br>' +
                            '<span style="font-size:16px">Dead: {point.dead}</span><br>' +
                            '<span style="font-size:16px">Active: {point.active}</span></p>',
                        positioner: function () {
                            return { x: 0, y: 450 };
                        },
                    },
                },
                {
                    type: 'mapbubble',
                    name: 'Dead',
                    data: [],
                    minSize: 3,
                    maxSize: '18%',
                    dataLabels: {
                        format: '{point.id}'
                    },
                    color: 'rgb(229, 62, 62)',
                    tooltip: {
                        backgroundColor: 'none',
                        borderWidth: 0,
                        shadow: false,
                        useHTML: true,
                        padding: 0,
                        pointFormat: '<p class="tooltip-bubble"><span style="font-size:20px">' +
                            '{point.province}, {point.country}</span> <br>' +
                            '<span style="font-size:16px">Dead: {point.z}</span><br></p>',
                        positioner: function () {
                            return { x: 0, y: 450 };
                        },
                    },
                },
                {
                    type: 'mapbubble',
                    name: 'Recovered',
                    data: [],
                    minSize: 3,
                    maxSize: '18%',
                    color: 'rgb(119, 152, 191)',
                    dataLabels: {
                        format: '{point.id}'
                    },
                    tooltip: {
                        backgroundColor: 'none',
                        borderWidth: 0,
                        shadow: false,
                        useHTML: true,
                        padding: 0,
                        pointFormat: '<p class="tooltip-bubble"><span style="font-size:20px">' +
                            '{point.province}, {point.country}</span> <br>' +
                            '<span style="font-size:16px">Recovered: {point.z}</span><br></p>',
                        positioner: function () {
                            return { x: 0, y: 450 };
                        },
                    },
                },
                {
                    name: 'Virus Spread',
                    type: 'mapline',
                    lineWidth: 1,
                    enableMouseTracking: false,
                    showInLegend: false,
                    data: []
                },
                {
                    type: 'mappoint',
                    visible: false,
                    data: [],
                    showInLegend: false,
                    enableMouseTracking: false
                },

                {
                    name: 'You',
                    type: 'mappoint',
                    visible: true,
                    showInLegend: false,
                    enableMouseTracking: false,
                    color: '#fff',
                    marker: {
                        lineWidth: 0,
                        radius: 6,
                        symbol: 'circle'
                    },
                    dataLabels: {
                        enabled: true,
                        align: 'right',
                        style: {
                            color: '#fff',
                            opacity: '0.9',
                            fontSize: '1.3em',
                            fontWeight: 'bold'
                        },
                        allowOverlap: false
                    },
                    data: []
                }
            ]
        })
        this.initSocketHandlers();
    },
    renderLocation: function (map) {
        var loc = window.localStorage.getItem('locationPointData');
        if (loc !== null) {
            try {
                map.series[6].addPoint(JSON.parse(loc), false, false, false);
            } catch (error) {
                console.log(error)
            }
        } else {
            $.ajax({
                url: "https://ipinfo.io",
                dataType: 'jsonp',
                crossDomain: true,
                data: function (geo) {
                    try {
                        var location = geo.loc.split(',')
                        var mapPointData = {
                            name: 'You',
                            lat: parseFloat(location[0]),
                            lon: parseFloat(location[1])
                        }
                        map.series[6].addPoint(mapPointData, false, false, false);
                        window.localStorage.setItem('locationPointData', JSON.stringify(mapPointData));
                    } catch (error) {
                        console.log(error)
                    }
                },
                timeout: 600
            });
        }
    },
    renderTable: function (data) {
        if ($.fn.dataTable.isDataTable('#country-table')) {
            table = $('#country-table').DataTable();
        }
        else {
            $('#country-table').DataTable({
                'pageLength': 50,
                data: data,
                columns: [
                    { title: 'Town', data: 'town' },
                    { title: 'Province', data: 'province' },
                    { title: 'Country', data: 'country' },
                    { title: 'Date', data: 'date' },
                    // { title: 'Lat', data: 'lat' },
                    // { title: 'Lon', data: 'lon' },
                    { title: 'Confirmed', data: 'confirmed' },
                    { title: 'Dead', data: 'dead' },
                    { title: 'Recovered', data: 'recovered' },
                    { title: 'Active', data: 'active' }
                ]
            });
        }
    },
    renderMap: function (data) {
        try {
            var hash = window.localStorage.getItem('mapHash');
            if (data !== null && ((hash == null || $('#map').hasClass('rendered') === false) || (typeof data.hash !== 'undefined' && typeof hash !== 'undefined' && hash !== data.hash))) {
                window.localStorage.setItem('mapHash', data.hash);
                data = data.cases;
                var soc = this;
                var map = $('#container').highcharts();
                data.forEach(function (location) {
                    if (location.confirmed > 0) {
                        map.series[1].addPoint({
                            id: location.id,
                            province: location.province,
                            country: location.country,
                            lat: location.lat,
                            lon: location.lon,
                            z: location.confirmed,
                            dead: location.dead,
                            recovered: location.recovered,
                            active: location.active,
                        }, false, false, false);
                    }
                    if (location.dead > 0) {
                        map.series[2].addPoint({
                            province: location.province,
                            country: location.country,
                            lat: location.lat,
                            lon: location.lon,
                            z: location.dead,
                        }, false, false, false);
                    }
                    if (location.recovered > 0) {
                        map.series[3].addPoint({
                            province: location.province,
                            country: location.country,
                            lat: location.lat,
                            lon: location.lon,
                            z: location.recovered,
                        }, false, false, false);
                    }
                })
                map.redraw()
                var mapPoint = map.get('hubei_china')
                var tmpCountry = '';
                var counter = 0;
                data.forEach(function (location) {
                    if (tmpCountry !== location.country) {
                        if (counter % 78 === 0) {
                            map.series[4].addPoint({
                                id: 'route',
                                path: Socket.pointsToPath(mapPoint, map.get(location.id))
                            }, false, false, false);
                            tmpCountry = location.country;
                        }
                    }
                    counter++;
                })
                $('#map').addClass('rendered')
                $('#data-time').empty()
                $('#data-time').html(new Date().toLocaleDateString())
                setTimeout(function () {
                    soc.clearSpinner();
                    soc.renderLocation(map);
                    map.redraw()
                    soc.renderTable(data)
                }, 2000);
            }
            console.log('map updated!')
        } catch (error) {
            console.log(error)
        }
    },
    renderSummary: function (data) {
        try {
            var hash = window.localStorage.getItem('summaryHash');
            if (data !== null && ((hash == null || $('#case-data').hasClass('rendered') === false) || (typeof hash !== 'undefined' && hash !== data.hash))) {
                console.log('summary update!')
                window.localStorage.setItem('summaryHash', data.hash);


                if (typeof data !== 'undefined' && data !== '') {
                    sentimentEl = $('#sentiment');
                    sentimentEl.empty();
                    sentimentEl.html(data.twitter.sentiment || '----');
                }
                $('#country-data').empty();
                $('#country-data-template').tmpl(data.topCountries.filter((month, idx) => idx < 5)).appendTo('#country-data');
                $('#case-data').empty();
                $('#case-data-template').tmpl(data).prependTo('#case-data');
                var result = (data.cases.countries * 100) / this.totalCountries;
                $('#countries-affected').html(this.totalCountries + '/' + data.cases.countries + ' (' + result.toFixed(2) + '%)');
                $('#case-data').addClass('rendered')
            }
        } catch (error) {
            console.log(error)
        }
    },
    handleNews: function (data) {
        try {
            var hash = window.localStorage.getItem('newsHash');
            if (data !== null && ((hash == null || $('#news-data').children().length == 0) || (typeof hash !== 'undefined' && hash !== data.hash))) {
                console.log('news updated!')
                window.localStorage.setItem('newsHash', data.hash);
                var news = data.news
                $('#news-preview').empty();
                if (news.length == 1) {
                    this.pushAlert(news[0])
                }
                $('#news-preview-template').tmpl(news[0]).appendTo('#news-preview');
                $('#news-template').tmpl(news).prependTo('#news-data');
                $('.lazy').Lazy();
                return;
            }
            return;
        } catch (error) {
            console.log(error)
        }
    },
    pushAlert: function (message) {
        if (typeof message !== 'undefined') {
            try {
                var notification = document.getElementById('notification');
                notification.play()
                Push.create('COVID-19 news!', {
                    body: message.title,
                    icon: 'img/apple-icon.png',
                    timeout: 10000,
                    vibrate: [300, 200, 100],
                    onClick: function () {
                        $('body main').each(function () {
                            $(this).hide();
                        });
                        $('.nav-item').each(function () {
                            $(this).removeClass('active');
                        });
                        $('.news').parent('li').addClass('active');
                        $('#news').show();
                        window.focus();
                    }
                });
            } catch (error) {
                console.log(error)
            }
        }
    },
    clearSpinner: function () {
        setTimeout(function () {
            $('#status').fadeOut('slow');
            $('#preloader')
                .delay(333)
                .fadeOut('slow')
                .remove();
        }, 100);
    }
}

$(document).ready(function () {
    Socket.initHandlers();
});