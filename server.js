const forge = require('node-forge');
const express = require('express');
const http = require('http');
const socketIo = require('socket.io')
const Tortoise = require('tortoise')
var oldCaseDataHash, oldSummaryDataHash = "";
const redis = require('redis');

// initialise redis
const redisClient = redis.createClient(process.env.REDIS_PORT || '6379', process.env.REDIS_HOST || 'localhost');
redisClient.on('connect', function () {
    console.log('Redis redisClient connected');
});
redisClient.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

// initialise rabbitMQ
const tortoise = new Tortoise(process.env.RABBIT_HOST || 'amqp://guest:guest@127.0.0.1:5672/')
tortoise.on(Tortoise.EVENTS.CONNECTIONCLOSED, () => {
    console.log('RabbitMQ connection closed')
})
tortoise.on(Tortoise.EVENTS.CONNECTIONDISCONNECTED, () => {
    console.log('RabbitMQ connection disconnected')
})
tortoise.on(Tortoise.EVENTS.CONNECTIONERROR, (err) => {
    console.log('RabbitMQ connection error', err)
})

// express
const port = process.env.PORT || 3000;
const app = express();
const server = http.createServer(app, '0.0.0.0');
server.listen(port);
const io = socketIo(server);
io.set({ 'transports': ['websocket'], "timeout": 10000 });
// const io = socketIo(server, { pingInterval: 60000, pingTimeout: 60000 });

const path = require('path');
const public = path.join(__dirname, '.');
let numClients = 0;

// API routes
app.get('/', function (req, res) {
    res.sendFile(path.join(public, 'index.html'));
});
app.use('/', express.static(public));
app.get('/news', function (req, res) {
    redisClient.get("news", function (error, result) {
        if (error) {
            console.log(error);
            throw error;
        }
        console.log('Returning /news')
        res.send(JSON.parse(result))
    });
});
app.get('/cases', function (req, res) {
    redisClient.get("cases", function (error, result) {
        if (error) {
            console.log(error);
            throw error;
        }
        console.log('Returning /map')
        res.send(JSON.parse(result))
    });
});
app.get('/summary', function (req, res) {
    redisClient.get("summary", function (error, result) {
        if (error) {
            console.log(error);
            throw error;
        }
        console.log('Returning /summary')
        res.send(JSON.parse(result))
    });
});

function sendHeartbeat() {
    setTimeout(sendHeartbeat, 8000);
    io.sockets.emit('ping', { beat: 1 });
}

io.on('connection', socket => {
    console.log('New connection from ' + socket.handshake.headers.host);
    // TODO: show ip of all live users map
    numClients++;
    console.log('Num clients: ' + numClients);
    socket.on('pong', function (data) {
        console.log("Pong received from client");
    });
    redisClient.get("cases", function (error, result) {
        if (error) {
            console.log(error);
            throw error;
        }
        socket.emit('cases', JSON.parse(result));
    });
    redisClient.get("news", function (error, result) {
        if (error) {
            console.log(error);
            throw error;
        }
        socket.emit('news', JSON.parse(result));
    });
    redisClient.get("summary", function (error, result) {
        if (error) {
            console.log(error);
            throw error;
        }
        socket.emit('summary', JSON.parse(result));
    });
    socket.on('disconnect', () => {
        numClients--;
        console.log('Client disconnected, num clients: ' + numClients);
    });
});


// listen to event queues
tortoise
    .queue(process.env.MESSAGE_QUEUE || 'news', { durable: false })
    .prefetch(1)
    .json()
    .subscribe((msg, ack, nack) => {
        if (msg) {
            redisClient.get("news", function (error, result) {
                if (error) {
                    console.log(error);
                    throw error;
                }
                io.of('/').emit('news', JSON.parse(result));
            });
            ack();
        }
    })
tortoise
    .queue(process.env.CASE_DATA || 'cases', { durable: false })
    .prefetch(1)
    .json()
    .subscribe((msg, ack, nack) => {
        if (msg) {
            redisClient.get("cases", function (error, result) {
                if (error) {
                    console.log(error);
                    throw error;
                }
                console.log("new case data notificaion")
                io.of('/').emit('cases', JSON.parse(result));
            });
            ack();
        }
    })
tortoise
    .queue(process.env.SUMMARY_DATA || 'summary', { durable: false })
    .prefetch(1)
    .json()
    .subscribe((msg, ack, nack) => {
        if (msg) {
            redisClient.get("summary", function (error, result) {
                if (error) {
                    console.log(error);
                    throw error;
                }
                console.log("new summary notificaion")
                io.of('/').emit('summary', JSON.parse(result));
            });
            ack();
        }
    })

// create a hash of the the input string
function hashCode(s) {
    var md = forge.md.sha1.create();
    md.update(s);
    return md.digest().toHex();
}



console.log('version: 0.0.1')
